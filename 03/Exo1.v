Set Implicit Arguments.

Inductive list (A: Type) : Type :=
nil : list A | cons : A -> list A -> list A.

Variable A : Type.

Fixpoint app (l1:list A) (l2:list A) : list A :=
  match l1 with
    | nil => l2
    | cons hd tl => cons hd (app tl l2)
end.

Fixpoint map (f : A -> A) (l : list A) : list A :=
  match l with
    | nil => nil A
    | cons hd tl => cons (f hd) (map f tl)
end.

Lemma appmap : forall f l l', map f (app l l') = app (map f l) (map f l').
Proof.
intros f l l'.
induction l. (* on veut faire la preuve par induction sur le l *)
trivial. (* regle *)
simpl. (* on calcule les app et les map *)
rewrite IHl.
trivial.
Qed.

Fixpoint filter (p : A -> bool) (l : list A) : list A  :=
  match l with
    | nil => nil A
    | cons hd tl => if p hd then cons hd (filter p tl) else filter p tl
end.
(* on a besoin que les deux conditions soient égales *)
Lemma filtermap : forall f p q l,
                  (forall a, p a = q (f a))
                   -> map f (filter p l) = filter q (map f l).
Proof.
intros f p q l.
intro H.
induction l.
trivial.
simpl.
case_eq (p a).
intro.
rewrite H in H0.
rewrite H0.
simpl.
f_equal.
trivial.
intro.
rewrite H in H0.
rewrite H0.
trivial.
Qed.


(* On commence par definir une fonction is_in qui renvoie vrai ssi un element
est dans la liste *)

Fixpoint is_in (a : A) (l : list A) : Prop :=
  match l with
    | nil => False
    | cons hd tl => a = hd \/ is_in a tl
end.

Definition equiv (l : list A) (l' : list A) : Prop :=
  forall a, is_in a l <-> is_in a l'
.

Lemma equiv_refl : forall l, equiv l l.
Proof.
intro.
unfold equiv.
intro.
split; intro; trivial.
Qed.

Lemma equiv_sym : forall l l', equiv l l' -> equiv l' l.
Proof.
intros l l'.
intro H.
unfold equiv in *.
intro a.
split; apply H.
Qed.

Lemma equiv_trans : forall l1 l2 l3, (equiv l1 l2 /\ equiv l2 l3) -> equiv l1 l3.
Proof.
intros l1 l2 l3.
intro H.
destruct H.
unfold equiv in *.
intro.
split.
intro.
apply H in H1.
apply H0.
assumption.
intro.
apply H0 in H1.
apply H.
assumption.
Qed.


Lemma equiv_app : forall l m1 m2, equiv l (app m1 m2) -> (forall a, equiv (cons a l) (app m1 (cons a m2))).
Proof.
intros l m1 m2.
intro H.
intro a.
unfold equiv in *.
intro a0.
split.
simpl.
intro.
destruct H0.
rewrite H0.
simpl.
induction m1.
simpl.
left.
trivial.
simpl.
right.
apply IHm1.
intros.
split.
intros.

Abort.


Lemma pika : forall 

Lemma equiv_app : forall l m1 m2, equiv l (app m1 m2) -> (forall a, equiv (cons a l) (app m1 (cons a m2))).
Proof.
intros.
unfold equiv in *.
simpl.
intros.
split.
intros.
Focus 2.
intros.
right.
apply H.
induction m1.
simpl.
inversion H0.
rewrite H1.

assert (is_in a0 (app m1 m2) -> is_in a0 (app m1 (cons a m2))).
intros.
assumption.

intros.


induction m2.
unfold equiv in *.
split.
intros.
Focus 2.
unfold equiv in *.
intros.
split.
intros.
inversion H0.
Focus 3.
intros.
.
