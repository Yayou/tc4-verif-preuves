Variable L : Type.
Variable betas : L -> L -> Prop.

Definition normal t : Prop := (* le terme t est en forme normale *)
(* le terme t n'est pas en forme normale : \exists u \in L, betas(t,u) \and \not(t=u) *)
(* negation de ca : \forall u \in L, betas(t,u) => t=u *)
forall u : L, betas t u -> (t=u)
.

Definition confluent : Prop := (* la relation betas est confluente *)
(* confluente : si on a deux termes, on peut trouver un terme atteignable par les 2 *)
(*  \forall t \in L, \forall u \in L, v \in L tq betas(t,u) \and betas (t,v),
\exists w \in L tq betas(u,w) \and betas (v,w) *)
forall t : L, forall u : L, forall v : L,
exists w : L,
and (betas t u) (and (betas t v) (and (betas u w) (betas v w)))
.

Definition has_normal  t : Prop := (* le terme t admet une forme normale *)
(* \exists v \in L tq betas (t,v) \and normal v *)
exists v : L, and (betas t v) (normal v)
.

Check (* propriété il existe des termes qui n'ont pas de forme normale *)
exists v : L, not (has_normal v)
.
