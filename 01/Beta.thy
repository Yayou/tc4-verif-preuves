theory Beta imports Main
begin

typedecl L

consts betas :: "L \<Rightarrow> L \<Rightarrow> bool"

definition normal :: "L \<Rightarrow> bool" where 
   "normal t = (\<forall> u. betas t u \<longrightarrow> t=u)"
   (* le terme t est en forme normale *)

definition confluent :: bool where 
   "confluent = (\<forall> t u v. \<exists> w. betas t u \<and> betas t v \<and> betas u w \<and> betas v w)"
   (* la relation betas est confluente *)

definition has_normal  :: "L \<Rightarrow> bool" where 
   "has_normal t = (\<exists> v. betas t v \<and> normal v)"
   (* le terme t admet une forme normale *)

prop (* propriété il existe des termes qui n'ont pas de forme normale *)
"\<exists> v. \<not> (has_normal v)"
end