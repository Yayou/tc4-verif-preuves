Variable ind : Type.
Variable res : Type.
Variable grpe : Type.
Variable act : Type.

Variable Lire : act.
Variable Ecrire : act.

Variable dans : ind -> grpe -> Prop.
Variable proprio : ind -> res -> Prop.
Variable droit : grpe -> act -> res -> Prop.
Variable peut : ind -> act -> res -> Prop.

Check (* si droit ecriture, alors droit lecture *)
forall x : ind, forall r : res, peut x Ecrire r -> peut x Lire r
.

Check (* quelqu'un a tous les droits *)
exists x : ind, forall a : act, forall r : res, peut x a r
.

Check (* proprio a tous les pouvoirs *)
forall x : ind, forall a : act, forall r : res, proprio x r -> peut x a r
.

Check (* un unique proprio *)
forall r : res, forall x : ind, forall y : ind, or (not (proprio x r)) (not (proprio y r))
.

Check (* aucun groupe n'est vide et tte personne appartient a au moins un groupe *)
and (forall g : grpe, exists x : ind, dans x g) (forall x : ind, exists g : grpe, dans x g)
.

Check (* autorisation groupe => autorisation ts membres groupe *)
forall g : grpe, forall a : act, forall x : ind, forall r : res, droit g a r -> dans x g -> peut x a r
.