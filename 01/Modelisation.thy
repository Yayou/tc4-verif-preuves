theory Modelisation imports Main
begin

typedecl ind
typedecl grpe
typedecl res
typedecl act

consts Lire :: act
consts Ecrire :: act

consts dans :: "ind \<Rightarrow> grpe \<Rightarrow> bool"
consts proprio :: "ind \<Rightarrow> res \<Rightarrow> bool"
consts droit :: "grpe \<Rightarrow> act \<Rightarrow> res \<Rightarrow> bool"
consts peut :: "ind \<Rightarrow> act \<Rightarrow> res \<Rightarrow> bool"

prop (* si un individu qcq a le droit d'ecriture, il a le droit de lecture*)
"\<forall> x r. peut x Ecrire r \<longrightarrow> peut x Lire r"

prop (* il existe qqn qui a ts les droits *)
"\<exists> x. \<forall> a r. peut x a r"

prop (* proprio a tous les droits *)
"\<forall> x a r. proprio x r \<longrightarrow> peut x a r"

prop (* toute ressource a un seul proprio *)
"\<forall> r x y. \<not> (proprio x r) \<or> \<not> (proprio y r)"

prop (* aucun groupe n'est vide et tte personne appartient a au moins un gp *)
"(\<forall> g. \<exists> x. dans x g) \<and> (\<forall> x. \<exists> g. dans x g)"

prop (* lorsqu'un groupe est autorisé à faire qqch, tt le monde dans le groupe peut le faire *)
"\<forall> g a x r. droit g a r \<longrightarrow> dans x g \<longrightarrow> peut x a r"
end