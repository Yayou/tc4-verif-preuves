Section Basic.
(*Basic Logical Reasoning *)

Variables A B : Prop.

Goal A /\ B -> B /\ A.
Proof.
intro.       (* Coq gere l'implication *)
destruct H.  (* on a en hypothese A et B. On en deduit que A, et B *)
split.       (* on veut A et B. Donc on veut A, et B *)
assumption.  (* B est vrai, d'apres les hyp *)
assumption.  (* A est vrai, d'apres les hyp *)
Qed.


(* Coq basic theory implements an intuitionistic logic which does not prove 
   Pierce law but a weaker version of it (not not P => P) is not a basic axiom *)

Variables P Q : Prop.

Lemma PierceI : ((P->Q)->P)-> ~ ~ P. 
Proof.
intro. (* on casse l'implication *)
intro. (* ~P <=> P => Faux donc on recasse l'implication *)
apply H0. (* cf ci-dessous *)
apply H. (* remplace P par P->Q dans le but *)
intro. (* on casse l'implication *)
exfalso. (* youhou, c'est contradictoire  *)
apply H0. (* mais du coup comme on sait que non P, prouver que faux revient a prouver que P *)
assumption. (* et P est vrai, par hyp *)
Qed.


(* A special library can be loaded to add classical reasoning *)
Require Import Classical.

Lemma Pierce : ((P->Q)->P)->P. 
(* Excursion: this is actually known under the name "Pierce Law".
        It is not a theorem in Pure (since Pure is intuitionistic), 
        but a theorem in HOL: *)
Proof.
intro.
apply NNPP. (* on applique la prop de classical *)
apply PierceI. (* on est sur le lemme d'au dessus *)      
assumption.
Qed.
 
End Basic.

Section Equational.

(* Very Basic Equational Reasoning *)
(* no automation at all: the example from the course *)
Variable E : Type.
Variables a b c:E.
Variable f : E*E -> E.
Variable g : E -> E.

Lemma A: f(a,b) = a -> f(f(a,b),b) = c -> g a = g c.
intro. (* on casse *)
rewrite H. (* on remplace f(a,b) par a dans le but *)
rewrite H. (* idem *)
intro. (* on casse *)
rewrite H0. (* on remplace a par c *)
trivial. (* gagne *)
Qed.

End Equational.

Section Xor.
Definition xor (A B :Prop) : Prop := (A /\ ~ B) \/ (~ A /\ B).

Lemma xor_refl A : xor A (~A).
Proof. (* requires classical reasoning *)
unfold xor.
destruct (classic A).
left.
split.
assumption.
intro.
apply H0.
assumption.
right.
split.
assumption.
assumption.
Qed.
End Xor.

Section Sets.
Variable E : Type.

Definition incl A B := forall x : E, A x -> B x.

Variable A B C : E -> Prop.

Lemma incl_trans : incl A B -> incl B C -> incl A C.
Proof.
intro. (* => *)
intro. (* => *)
unfold incl. (* incl A C -> \forall x : E, A x -> B x *)
intro. (* \forall *)
intro. (* => *)
unfold incl in X. (* incl A B -> idem *)
unfold incl in X0. (* incl B C -> idem *)
apply X0. 
apply X.
assumption.
Qed.
End Sets.

Section FirstOrder.
Variable E : Type.
Variable A : Prop.
Variable B : E -> Prop.

Lemma all_istr: 
    (forall x, A -> B x) <-> (A  -> forall x, B x).
Proof.
split. (* <=> = <= et => *)
intro. (* on casse l'implication *)
intro. (* idem *)
intro. (* on gere le \forall x *)
apply H. (* l'hyp est vraie *)
assumption. (* bingo *)
intro. (* => *)
intro. (* \forall *)
intro. (* => *)
apply H. (* l'hyp est vraie *)
assumption. (* bingo *)
Qed.

Variable P : E -> E -> Prop.

Lemma ex_all : (forall x, exists y, P x y) -> forall y, exists x, P x y.
Proof.
(* Possible to prove ? *)

Abort.

(* Coq does not assume that Type are non-empty *)

Variable e:E.
Lemma ex_all2 : (forall x, forall y, P x y) -> exists y, forall x, P x y.
Proof.
intro.
exists (e).
intro.
apply H.
Qed.


End FirstOrder.
