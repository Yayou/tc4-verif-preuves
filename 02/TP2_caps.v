Section Basic.
(*Basic Logical Reasoning *)

Variables A B : Prop.

Goal A /\ B -> B /\ A.
Proof.
intros (Ha, Hb); split; assumption.
intro.       (* Coq gere l'implication *)
destruct H.  (* on a en hypothese A et B. On en deduit que A, et B *)
split.       (* on veut A et B. Donc on veut A, et B *)
assumption.  (* B est vrai, d'apres les hyp *)
assumption.  (* A est vrai, d'apres les hyp *)
Qed.

(* de même si tu as une hypothèse du type :
( existe b, Pb) -> blablabla, tu peux mettre 
intros (b, Hb). pour les détacher directement.
logique. 

ah tu pouvait faire auusi intros (Ha, Hb); intuition.
non, on fait pas de magie noire :p

intros; intuition.
laisse mes cinq lignes tranquilles :p

intuition tout seul ça marche  ... non..... :p

mais du coup le ; i fait quoi la ? parce qu'on a jamais qu'un seul but, nope ?

il fait ça en une seul intruction et pas deux, enfin plus en un temps et pas deux
tu clique qu'une fois sur le truc pour changer d'instruction (tu pouvais dire la fleche ;))
okiiii ... par contre on a pas le detail, donc au placard pour l'instant la formule magique :D*)

(* Coq basic theory implements an intuitionistic logic which does not prove 
   Pierce law but a weaker version of it (not not P => P) is not a basic axiom *)

Variables P Q : Prop.

Lemma PierceI : ((P->Q)->P)-> ~ ~ P. 
Proof.
intros H G. (* deux pour le prix d'un*)
apply G, H. (* de même *)
intros Hf. 
contradiction. (* deux hypothèse se contredisent donc ca résoud le but.*)
intro. (* on casse l'implication *)
intro. (* ~P <=> P => Faux donc on recasse l'implication *)
apply H0. (* cf ci-dessous *)
apply H. (* remplace P par P->Q dans le but *)
intro. (* on casse l'implication *)
exfalso. (* youhou, c'est contradictoire  *)
apply H0. (* mais du coup comme on sait que non P, prouver que faux revient a prouver que P *)
assumption. (* et P est vrai, par hyp *)
Qed.


(* A special library can be loaded to add classical reasoning *)
Require Import Classical.

Lemma Pierce : ((P->Q)->P)->P. 
(* Excursion: this is actually known under the name "Pierce Law".
        It is not a theorem in Pure (since Pure is intuitionistic), 
        but a theorem in HOL: *)
Proof.
intro.

now apply NNPP, PierceI. marche aussi
le [now X.] fait office de [X; auto. ]. et auto fait quoi ?
un mélange des tactiques symmetry, trivial, reflexivity, et toutes les truc a peux près basique de la logique

apply NNPP. (* on applique la prop de classical *)
apply PierceI. (* on est sur le lemme d'au dessus *)      
assumption.
Qed.

End Basic.

Section Equational.

(* Very Basic Equational Reasoning *)
(* no automation at all: the example from the course *)
Variable E : Type.
Variables a b c:E.
Variable f : E*E -> E.
Variable g : E -> E.



Lemma A: f(a,b) = a -> f(f(a,b),b) = c -> g a = g c.
intro. (* on casse *)
ici tu peux faire rewrite H,H. ? oui mais aussi do 2 rewrite H. voir [ repeat rewrite H.] mais c'est dangereux car ca peux boucler a l'infini ^^.
rewrite H. (* on remplace f(a,b) par a dans le but *)
rewrite H. (* idem *)
intro. (* on casse *)
rewrite H0. (* on remplace a par c *)
trivial. (* gagne *) ici reflexivity marche aussi et t'explique peut etre mieux ^^
en gros trivial c'est le auto du pauvre
et au dessus il y a intuition.
et reflexivity i fait quoi ?
si tu a [ a = a] tu a gagner, un coté est la réfléxion de l'autre...
Qed.

End Equational.

Section Xor.
Definition xor (A B :Prop) : Prop := (A /\ ~ B) \/ (~ A /\ B).

Lemma xor_refl A : xor A (~A).
Proof. (* requires classical reasoning *)

unfold xor.
destruct (classic A).
left.
split; auto.
right.
split; assumption.
(* juste de la réduction de taille, mais pas vraiment de tactiques mieux ^^ *)
un seul assumption suffit ? avec le point virgule oui oki en gros tu applique assumption a tout les buts générés par split aaaaah. et comme i genere deux buts dont on a déjà les hypothèses, on assumption les deux et bingo !

unfold xor.
destruct (classic A).
left.
split.
assumption.
intro.
apply H0.
assumption.
right.
split.
assumption.
assumption.
Qed.
End Xor.

Section Sets.
Variable E : Type.

Definition incl A B := forall x : E, A x -> B x.

Variable A B C : E -> Prop.

Lemma incl_trans : incl A B -> incl B C -> incl A C.
Proof.
unfold incl in *. (* on développe*)
intros Hab Hbc x Ha. (* on introduit la rpemiere hyp, la deuxième, le x et le (A x).
specialize (Hab x).  (* on dit que dans le (forall x, A x -> B x), on prend en particulier le x des hypothèses. c'est un peu confu car il y a dans les deux cas des x, mais on aurrait pus introduire en y, et fait [specialize (Hab y)].
specialize (Hbc x).
apply Hbc, Hab; assumption. (* on applique juste les deux truc a la suite, puis on se retrouve avec un truc qui est dans les hypothèses donc on assume directe. *)

bon courage, mais c'est déja plus de haut niveau ça ^^

intro. (* => *)
intro. (* => *)
unfold incl. (* incl A C -> \forall x : E, A x -> B x *)
intro. (* \forall *)
intro. (* => *)
unfold incl in X. (* incl A B -> idem *)
unfold incl in X0. (* incl B C -> idem *)
apply X0. 
apply X.
assumption.
Qed.
End Sets.

Section FirstOrder.
Variable E : Type.
Variable A : Prop.
Variable B : E -> Prop.

Lemma all_istr: 
    (forall x, A -> B x) <-> (A  -> forall x, B x).
Proof.
split; intros; apply H; assumption.
split. (* <=> = <= et => *)
intro. (* on casse l'implication *)
intro. (* idem *)
intro. (* on gere le \forall x *)
apply H. (* l'hyp est vraie *)
assumption. (* bingo *)
intro. (* => *)
intro. (* \forall *)
intro. (* => *)
apply H. (* l'hyp est vraie *)
assumption. (* bingo *)
Qed.

Variable P : E -> E -> Prop.

Lemma ex_all : (forall x, exists y, P x y) -> forall y, exists x, P x y.
Proof.
(* Possible to prove ? *)

Abort.

(* Coq does not assume that Type are non-empty *)

Variable e:E.
Lemma ex_all2 : (forall x, forall y, P x y) -> exists y, forall x, P x y.
Proof.
intro.
exists (e).
intro.
apply H.
Qed.


End FirstOrder.


