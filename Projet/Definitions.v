(** Question 1 **)
(** Definition des differents elements de la representation **)
Close Scope nat.
Require Export MSets.
Require Export Arith NArith.

Open Local Scope list.

Definition var : Type := N.
Print var.
Definition litt : Type := (var*bool).
Print litt.

Module litt_as_OT <: OrderedTypeWithLeibniz.

Definition t : Type := litt.

Definition eq (l1:t) (l2:t) : Prop := l1 = l2.
Lemma eq_equiv : Equivalence eq.
split; red.
*
intro x.
unfold eq.
trivial.
*
intros x y H.
unfold eq in *.
intuition.
*
intros x y z H1 H2.
unfold eq in *.
rewrite H1.
assumption.
Qed.

Definition lt (l1 : t) (l2 : t) :=
  N.lt (fst l1) (fst l2)
  \/ (fst l1 = fst l2 /\ snd l1 = false /\ snd l2 = true)
.
Lemma lt_strorder : StrictOrder lt.
split; repeat red.
*
intros x H.
unfold lt in *.
destruct H.
apply (N.lt_irrefl _ H).
destruct H as (_,(H1,H2)).
rewrite H1 in H2.
discriminate.
*
intros x y z H1 H2.
unfold lt in *.
destruct H1 as [H1a|H1b].

destruct H2 as [H2a|H2b].
left.
rewrite H2a in H1a.
assumption.
left.
destruct H2b as (H2b,_).
rewrite H2b in H1a.
assumption.

destruct H2 as [H2a|H2b].
destruct H1b as (H1b,_).
rewrite H1b.
left.
assumption.
right.
repeat split.
destruct H1b as (H1,_).
destruct H2b as (H2,_).
rewrite H1.
assumption.
destruct H1b as (_,(H1,_)).
assumption.
destruct H2b as (_,(_,H2)).
assumption.
Qed.

Lemma lt_compat : Proper (eq ==> eq ==> iff) lt.
repeat red.
intros x y H1 x0 y0 H2.
split.
*
intro H3.
unfold lt in *.
unfold eq in *.
destruct H3; rewrite H1 in H; rewrite H2 in H.
left.
assumption.
right.
assumption.
*
unfold lt in *.
unfold eq in *.
intro H3.
destruct H3 as [H3a|H3b]; rewrite H1; rewrite H2.
left.
assumption.
right.
assumption.
Qed.

Definition compare (l1 l2 : t) : comparison :=
  match N.compare (fst l1) (fst l2) with
  | Lt => Lt
  | Gt => Gt
  | Eq => match snd l1, snd l2 with
          | true, true | false, false => Eq
          | true, false => Gt
          | false, true => Lt
          end
  end
.
Lemma compare_spec :
  forall x y : t,
  CompareSpec (eq x y) (lt x y) (lt y x) (compare x y).
intros x y.
induction compare.
*



admit.
Admitted.

Lemma eq_dec : forall x y : t, {eq x y} + {~ eq x y}.
unfold eq.
decide equality.
decide equality.
decide equality.
decide equality.
Qed.
Lemma eq_leibniz : forall x y : t, eq x y -> x = y.
intros x y H.
unfold eq in H.
assumption.
Qed.

End litt_as_OT.

Module clause <:SWithLeibniz
   := MSetList.MakeWithLeibniz(litt_as_OT).

Module partial_int <: SWithLeibniz
   := MSetList.MakeWithLeibniz(litt_as_OT).

Module prop <: WSets
   := MSetWeakList.Make(clause).

Module solution <: WSets
   := MSetWeakList.Make(partial_int).



Open Scope N.
Definition x : var := 4.
Print x.

Definition l : litt := (4,false).
Print l.

Definition empty := clause.empty.
Definition one := clause.add (5,true) empty.
Definition two := clause.add (2,true) one.
Definition three := clause.add (6,false) two.
Eval compute in clause.cardinal three. (* donne 3%nat *)
Eval compute in clause.min_elt three. (* donne (2, true) *)
Eval compute in clause.max_elt three. (* donne (6, false) *)
Eval compute in clause.elements three.

Definition int := partial_int.add (1,false) (partial_int.add (2,true) (partial_int.singleton (0,true))).
Eval compute in partial_int.elements int.

Definition p := prop.add three (prop.singleton two).
Eval compute in prop.choose p.
Eval compute in prop.elements p.


(** Question 2 **)
(** Transformation des règles **)

Definition neg_litt (l:litt) : litt := (fst l, negb(snd l)).

Definition notin_clause (t:litt) (c:clause.t) : bool :=
if (clause.mem t c) then false else true.

Definition filt_litt (l:litt) (c:clause.t) (p:prop.t) : prop.t :=
  prop.add (clause.remove l c) p.

Inductive DPLL : partial_int.t -> prop.t -> solution.t -> Prop :=
    Success :
      forall I,
      DPLL I prop.empty (solution.singleton I)
  | Conflict :
      forall I D,
      DPLL I (prop.add clause.empty D) solution.empty
  | BcpV :
      forall I D S C n b,
      DPLL I D S
      /\ partial_int.In (n,b) I
      /\ not(clause.In (n,b) C)
      -> DPLL I (prop.add (clause.add (n,b) C) D) S
  | BcpF :
      forall I D S C n b,
      DPLL I (prop.add C D) S
      /\ partial_int.In (n, negb(b)) I
      /\ not(clause.In (n,b) C)
      -> DPLL I (prop.add (clause.add (n,b) C) D) S
  | AssumeV :
      forall I D S n,
      DPLL (partial_int.add (n,true) I) D S
      /\ (forall b, not(partial_int.In (n, b) I))
      -> DPLL I (prop.add (clause.singleton (n,true)) D) S
  | AssumeF :
      forall I D S n,
      DPLL (partial_int.add (n,false) I) D S
      /\ (forall b, not(partial_int.In (n,b) I))
      -> DPLL I (prop.add (clause.singleton (n,false)) D) S
  | Unsat :
      forall I D S1 S2 n,
      DPLL (partial_int.add (n,true) I) D S1
      /\ DPLL (partial_int.add (n,false) I) D S2
      /\ (forall b, not(partial_int.In (n,b) I))
      -> DPLL I D (solution.union S1 S2)
.


(** Question 3 **)
(** Verification du bon fonctionnement de DPLL **)

Lemma in_empty_sol : forall e, not(solution.In e solution.empty).
Proof.
admit.
Admitted.

Lemma in_empty_prop : forall e, not(prop.In e prop.empty).
Proof.
admit.
Admitted.

Lemma subset_sol :
  forall I D sol J n b,
  DPLL I D sol /\ solution.In J sol /\ partial_int.In (n,b) I -> partial_int.In (n,b) J.
Proof.
intros I D sol J n b and.
destruct and as (DPLL, (J_in_sol, nb_in_I)).
induction DPLL.

* (** Success **)
apply solution.singleton_spec in J_in_sol.
unfold partial_int.Equal in J_in_sol.
apply J_in_sol.
assumption.

* (** Conflict **)
apply in_empty_sol in J_in_sol.
exfalso.
assumption.

* (** BcpV **)
destruct H as (DPLL, (l0_in_I, l0_notin_clause)).
admit.

* (** BcpF **)
destruct H as (DPLL, (negl0_in_I, l0_notin_cla)).
admit.

* (** AssumeV **)
destruct H as (DPLL, n_notin_I).
admit.

* (** AssumeF **)
destruct H as (DPLL, n_notin_I).
admit.

* (** Unsat **)
destruct H as (DPLL_true, (DPLL_false, n_notin_I)).
apply solution.union_spec in J_in_sol.
destruct J_in_sol as [J_in_sol1|J_in_sol2].
admit.


Admitted.




Lemma correction :
forall I D S,
DPLL I D S ->
  forall J, solution.In J S ->
    (** pour toutes var dans I, elle a la même val dans J **)
    (partial_int.Subset I J
    (** J rend vraies toutes les clauses de D **)
    /\ (forall c, prop.In c D -> exists n b, clause.In (n,b) c /\ partial_int.In (n,b) J))
.
Proof.
intros I D S DPLL J.
intro J_in_S.
split.

*
red.
intro elt.
elim elt.
intros a b ab_in_I.
apply subset_sol with I D S.
split.
assumption.
split; assumption.

*
intros c c_in_D.
induction DPLL.

+
apply in_empty_prop in c_in_D.
exfalso.
assumption.

+
apply in_empty_sol in J_in_S.
exfalso.
assumption.

+
exists n.
exists b.
split.
apply prop.add_spec in c_in_D.
destruct c_in_D as [Eq|In].
unfold clause.Equal in Eq.
apply Eq.
apply clause.add_spec.
left.
trivial.
destruct H as (DPLL, (l0_in_I, l0_notin_clause)).
admit.
destruct H as (DPLL, (l0_in_I, l0_notin_clause)).
apply subset_sol with I D S.
split.
assumption.
split ; assumption.
+
exists n.
exists b.
split.
-
destruct H as (DPLL, (n_in_I, n_notin_C)).
apply prop.add_spec in c_in_D.
destruct c_in_D as [Eq|In].
unfold clause.Equal in Eq.
apply Eq.
apply clause.add_spec.
left.
trivial.
admit.

-
destruct H as (DPLL, (oppn_in_I, n_notin_C)).
apply prop.add_spec in c_in_D.
admit.

+
exists n.
destruct H as (DPLL, b_notin_I).
exists true.
split.
apply prop.add_spec in c_in_D.
destruct c_in_D as [Eq|In].
unfold clause.Equal in Eq.
apply Eq.
apply clause.singleton_spec.
trivial.
admit.
admit.

+
exists n.
exists false.
split.
apply prop.add_spec in c_in_D.
destruct c_in_D as [Eq|In].
unfold clause.Equal in Eq.
apply Eq.
apply clause.singleton_spec.
trivial.
admit.
admit.

+
destruct H as (DPLL1, (DPLL2, n_notin_I)).
apply solution.union_spec in J_in_S.
exists n.
admit.
Admitted.

Lemma completion :
forall I D,
DPLL I D solution.empty ->
  forall J,
  (partial_int.Subset I J
  /\ (forall n b1 b2,
      prop.exists_ (clause.mem (n,b1)) D = true
      -> partial_int.In (n,b2) J))
  ->
    exists c, prop.In c D -> forall l,
                             clause.In l c 
                             -> partial_int.In (neg_litt l) J
.
Proof.
intros I D DPLL J H.
destruct H as (I_sub_J, H2).
unfold partial_int.Subset in I_sub_J.

induction DPLL.

*
exists (clause.singleton (1,true)).
intro False.
apply in_empty_prop in False.
exfalso.
assumption.

*
admit.

*
exists C.
intros In l0 l0_in_C.
intuition.


admit.
Admitted.

Lemma terminaison : forall I D, exists sol, DPLL I D sol.
Proof.
intros I D.
admit.
Admitted.













