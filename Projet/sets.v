Set Implicit Arguments.

Require Export Arith NArith.

Require Export MSets.

Print Module Type MSetInterface.S.

Local Open Scope list.
Local Open Scope N_scope.

Definition var := N.

Module varSet <:MSetInterface.S := MSetList.Make(N_as_OT).

Record lit := {name : var; pos : bool}.

Module lit_as_OT <: OrderedTypeWithLeibniz.

Definition t : Type := lit.
Definition eq : t -> t -> Prop := @eq lit.
Definition eq_equiv : Equivalence eq := @eq_equivalence lit.
Definition lt l1 l2 : Prop := 
    N.lt (name l1) (name l2) 
\/ (name l1 = name l2 /\ pos l1 = false /\ pos l2 = true).
Lemma lt_strorder : StrictOrder lt.
split.
+ repeat red.
destruct 1.
apply (N.lt_irrefl _ H).
destruct H as (_,(H1,H2)).
rewrite H1 in H2; discriminate.
+ repeat red; intros.
admit.
Qed.

Lemma lt_compat : Proper (eq ==> eq ==> iff) lt.
admit.
Qed.
   
Definition compare (l1 l2: t) : comparison := Lt.
   
Lemma compare_spec :
     forall x y : t, CompareSpec (eq x y) (lt x y) (lt y x) (compare x y).
admit.
Qed.
   
Lemma eq_dec : forall x y : t, {eq x y} + {~ eq x y}.
unfold eq.
decide equality.
decide equality.
decide equality.
decide equality.
Defined.
   
Lemma eq_leibniz : forall x y : t, eq x y -> x = y.
trivial.
Save.

End lit_as_OT.

Module litSet <:SWithLeibniz
   := MSetList.MakeWithLeibniz(lit_as_OT).

