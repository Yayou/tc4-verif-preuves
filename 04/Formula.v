Set Implicit Arguments.

Require Export Interpretation.

(** * Propositional formulas *)

Definition atom := bool.

(** ** Syntax *)
Inductive binop := And | Or | Impl.

(* inductive definition of abstract syntax trees for formula *)

Inductive form := 
     Var : var -> form
   | Atom : atom -> form
   | Neg : form -> form
   | Bin : binop -> form -> form -> form.

(** Notations *)

Notation top := (Atom true).
Notation bot := (Atom false).
Notation negp := Neg.
Notation andp := (Bin And).
Notation orp := (Bin Or).
Notation implp := (Bin Impl).

(** Test cases *)

Definition p0 := Var 0.
Definition p1 := Var 1.
Definition p2 := Var 2.

Definition P0 := implp p0 p1.
Definition P1 := implp p1 P0.
Definition dnegp P Q := implp (implp P Q) Q.
Definition P2 := dnegp P0 p0.

(** ** Semantic *)
(* Boolean interpretation of binary operators *)

Definition interpbin (p:binop) : bool -> bool -> bool := 
     match p with And => andb | Or => orb |  Impl => implb end.

Fixpoint interp (I:interpretation) (P:form) : bool := 
     match P with (Var x) => I x
                | (Atom a) => a
                | (Neg Q)  => negb (interp I Q)
                | (Bin o Q R) => interpbin o (interp I Q) (interp I R)
     end.

(* ** Tests *)
Eval compute in (interp Itrue P2).
Eval compute in (interp Ifalse P2).
Eval compute in (interp (interpl (true::false::nil)) P2).
Eval compute in (interp (interpl (false::true::nil)) P2).

Definition eval2 A it (P : A) : list bool := 
  (it Itrue P)::(it Ifalse P)::
  (it (interpl (true::false::nil)) P)::
  (it (interpl (false::true::nil)) P)::nil.
Eval compute in (eval2 interp P2).

Definition truthtable2 A it (P : A) : list (bool * bool * bool) := 
  (true,true,it Itrue P)::(false,false,it Ifalse P)::
  (true,false,it (interpl (true::false::nil)) P)::
  (false,true,it (interpl (false::true::nil)) P)::nil.


Eval compute in (truthtable2 interp (orp (negp p0) p1)).
Eval compute in (truthtable2 interp (andp (negp p0) p1)).
Eval compute in (truthtable2 interp (implp (negp p0) p1)).

(** ** Equivalence *)

Definition equiv P Q := forall I, interp I P = interp I Q.

Lemma negimpl : forall P Q, equiv (negp (implp P Q)) (andp P (negp Q)).
intros P Q I.
simpl.
destruct (interp I P); reflexivity.
Qed.

Lemma implor : forall P Q, equiv (implp P Q) (orp (negp P) Q).
Proof.
intros P Q I.
simpl.
destruct (interp I P); trivial.
Qed.

(** ** Validity *)
Definition valid P := forall I, interp I P = true.

Lemma Pierce : forall P Q, valid (implp (implp (implp P Q) P) P).
Proof.
intros P Q I; simpl.
destruct (interp I P); trivial.
destruct (interp I Q); trivial.
Qed.


