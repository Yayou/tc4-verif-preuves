Set Implicit Arguments.


Require Export Bool List Arith.
Require Export Omega.

Definition var := nat.

Definition interpretation := var -> bool.

Definition update I x b : interpretation := 
    fun y => if beq_nat x y then b else (I y).

Lemma update_eq : forall I x b y, 
    x=y -> update I x b y = b.
Proof.
intros.
unfold update.
rewrite H.
rewrite <- beq_nat_refl.
trivial.
Qed.

Lemma update_neq : forall I x b y, 
   x<>y -> update I x b y = I y.
intros; unfold update.
rewrite <- beq_nat_false_iff in H.
rewrite H.
trivial.
Qed.
  
(* interpretation from a liste l of boolean values for variables 0;1;.. 
  followed by a default value d *)

Fixpoint interpld l d : interpretation := match l with 
   nil => fun x => d 
 | (a :: m) => fun x => match x with O => a 
                        | S n => interpld m d n end
 end.

Definition interpl l := interpld l true.

Definition Ifalse : interpretation := fun x => false.
Definition Itrue : interpretation := fun x => true.

