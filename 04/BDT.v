Set Implicit Arguments.

Require Export Interpretation.

(** * Binary decisions trees *)

Definition atom := bool.

Inductive bdt := 
   | Atomt : atom -> bdt
   | IFt : var -> bdt -> bdt -> bdt.

(** Notations *)

Notation topt := (Atomt true).
Notation bott := (Atomt false).
Definition vart n := IFt n topt bott.

(** Semantics *)

Fixpoint interpt (I:interpretation) (P:bdt) : bool := 
     match P with 
                | (Atomt a) => a
                | (IFt x P Q) => if I x then interpt I P else interpt I Q
     end.

(** tests *)

Definition Pt := IFt 0 (IFt 1 topt topt) (IFt 1 topt topt).

Eval compute in (interpt Itrue Pt).
Eval compute in (interpt Ifalse Pt).
Eval compute in (interpt (interpl (true::false::nil)) Pt).
Eval compute in (interpt (interpl (false::true::nil)) Pt).

(* Trivial equality to be used for rewriting *)

Lemma interpt_If : forall I x P Q, 
   interpt I (IFt x P Q) = if I x then interpt I P else interpt I Q.
Proof.
trivial.
Qed.

(** Logical operations on BDT *)

Fixpoint negt (t:bdt) : bdt := match t with 
   Atomt a => Atomt (negb a)
 | IFt x l r => IFt x (negt l) (negt r)
 end.

Lemma negt_corr : forall I P, 
    interpt I (negt P) = negb (interpt I P).
Proof.
induction P.
 - simpl.
 trivial.
 - simpl.
 destruct (I v).
trivial.
trivial.
Qed.

Section BinaryCombination.
Variable op : atom -> atom -> atom.

(** the following intuitive definition is not directly accepted 
Fixpoint bint (t u: bdt) : bdt := 
  match t with 
     Atomt a =>  match u with Atomt b => Atomt (op a b) 
                           |  IFt x l r => IFt x (bint t l) (bint t r)
                 end
   | IFt x l r => match u with Atomt b => IFt x (bint l u) (bint r u) 
                             |  IFt y l' r' => match nat_compare x y with 
                                 Lt => IFt x (bint l u) (bint r u) 
                                |Eq => IFt x (bint l l') (bint r r') 
                                |Gt => IFt y (bint t l') (bint t r') end
                  end
  end.

We use two fixpoints *)

Fixpoint bint (t u: bdt) : bdt := 
  match t with 
     Atomt a => let fix brect (u:bdt) : bdt := 
                   match u with Atomt b => Atomt (op a b) 
                             |  IFt x l r => IFt x (brect l) (brect r)
                   end
               in brect u
   | IFt x l r => let fix brect (u:bdt) : bdt (* computes bint t u recursively on u *)
                   := 
                   match u with Atomt b => IFt x (bint l u) (bint r u) 
                             |  IFt y l' r' => match nat_compare x y with 
                                 Lt => IFt x (bint l u) (bint r u) 
                                |Eq => IFt x (bint l l') (bint r r') 
                                |Gt => IFt y (brect l') (brect r') end
                   end
               in brect u
  end.

(** The equivalence with the first definition is trivially proven *)

Lemma bint_eq : forall t u, bint t u = 
match t with 
     Atomt a =>  match u with Atomt b => Atomt (op a b) 
                           |  IFt x l r => IFt x (bint t l) (bint t r)
                 end
   | IFt x l r => match u with Atomt b => IFt x (bint l u) (bint r u) 
                            |  IFt y l' r' => match nat_compare x y with 
                                 Lt => IFt x (bint l u) (bint r u) 
                                |Eq => IFt x (bint l l') (bint r r') 
                                |Gt => IFt y (bint t l') (bint t r') end
                 end
    end.
Proof.
destruct t; destruct u; trivial.
Qed.

End BinaryCombination.

(** Instanciation on standard boolean operations *)
Definition andt := bint andb.
Definition ort := bint orb.
Definition implt := bint implb.


(** formula examples *)

Definition p0t := vart 0.
Definition p1t := vart 1.
Definition P0t := implt p0t p1t.
Definition P1t := implt p1t P0t.
Definition dnegt P Q := implt (implt P Q) Q.
Definition P2t := dnegt P0t p0t.

Eval compute in P1t.
Eval compute in P2t.


(** Correctness of binary combination *)
Lemma bint_corr : forall f I P Q, 
    interpt I (bint f P Q) = f (interpt I P) (interpt I Q).
induction P.
-induction Q; rewrite bint_eq.
 +trivial.
 +rewrite interpt_If.
  rewrite interpt_If.
  destruct (I v); trivial.
-induction Q; rewrite bint_eq.
 +simpl.
  destruct (I v).
  apply IHP1.
  apply IHP2.
 + destruct (nat_compare_spec v v0).
   * rewrite H; repeat rewrite interpt_If.
     destruct (I v0); auto.
   * repeat rewrite (interpt_If I v).
     destruct (I v); auto.
   * repeat rewrite (interpt_If I v0).
     destruct (I v0); auto.
Qed.


Lemma ort_corr : forall I P Q, 
    interpt I (ort P Q) = (interpt I P) || (interpt I Q).
Proof.
exact (bint_corr orb).
Qed.

Fixpoint existt (x:var) (t:bdt) :=  
  match t with 
     Atomt a => t
   | IFt y l r => match nat_compare x y with 
                                 Lt => t (* x does not occur in t *)
                                |Eq => ort l r 
                                |Gt => IFt y (existt x l) (existt x r)                    
                 end
  end.

(** ordered n i t : 
   all variables x in t are increasing on branches and i <= x < n *)

Inductive ordered (n:nat) : nat -> bdt -> Prop := 
    Oatomt : forall i a, ordered n i (Atomt a)
  | Oift : forall i x l r, (i <= x < n) -> ordered n (S x) l -> ordered n (S x) r 
              -> ordered n i (IFt x l r).

Hint Constructors ordered.

Lemma negt_ord : forall i n P, ordered n i P ->  ordered n i (negt P).
Proof.
intros i n P H.
induction H.
- simpl.
apply Oatomt.
-
simpl.
apply Oift.
assumption.
assumption.
assumption.
Qed.

Lemma bint_ord : forall f i n P, ordered n i P -> forall Q, ordered n i Q
                 -> ordered n i (bint f P Q).
Proof.
induction 1.
- induction 1.
  + simpl.
    auto.
  + rewrite bint_eq.
    auto. 
- induction 1.
  + rewrite bint_eq; auto.
  + rewrite bint_eq.
    destruct (nat_compare_spec x x0).
    * subst x; auto.
    * auto with zarith.
    * auto with zarith.
Qed.

Lemma ordered_update : forall I x b i n t, ordered n i t -> (x < i \/ n <= x) ->
      interpt (update I x b) t = interpt I t.
induction 1.
-simpl.
 trivial.
-simpl.
intros.
rewrite update_neq; try omega.
rewrite IHordered1; try omega.
rewrite IHordered2; try omega.
trivial.
Qed.

Lemma orb_idem : forall x, x = x || x.
destruct x; trivial.
Qed.
Hint Resolve orb_idem.

Lemma existt_corr : forall i n I (x:var) (t:bdt), ordered n i t ->
  interpt I (existt x t) = orb (interpt (update I x true) t) (interpt (update I x false) t).
induction 1.
- simpl.
  auto.
- simpl.
destruct (nat_compare_spec x x0).
 + rewrite update_eq; trivial.
   rewrite update_eq; trivial.
   rewrite ort_corr.
   rewrite ordered_update with (1:=H0); try omega.
   rewrite ordered_update with (1:=H1); try omega.
   trivial.
 + rewrite update_neq; try omega.
   rewrite update_neq; try omega.
   do 2 (rewrite ordered_update with (1:=H0); try omega).
   do 2 (rewrite ordered_update with (1:=H1); try omega).
   simpl.
   destruct (I x0); auto.
 + rewrite update_neq; try omega.
   rewrite update_neq; try omega.
   simpl.
   destruct (I x0); auto.
Qed.
 

(** * Using BDT to encode sets of states and do symbolic model-checking *)

(** We start from a (finite) problem where states depend on a set of boolean variables x0..x(n-1) 
    Each state is represented as a formula over these variables corresponding to a bdt ordered between 
    0 and n.
    Each set of states can also be represented by a formula over the same variables 
    (takes the disjunction of all sets)

    The transition system is represented by a formula with 2n variables corresponding to the values 
    before (x_i) and after (x_(n+i)). 
       - A single transition between state Gi(x_0,..,x_(n-1)) and Gj(x_0,..,x_(n-1))
    becomes a formula T_(ij)(x_0,..,x_(2n-1))=G1(x_0,..,x_(n-1))/\G2(x_n,..,x_(2n-1))
    The transition system is a formula T(x_0,..,x_(2n-1))
    corresponding to the disjonction of all transitions.

    We can now compute a set of states leading to a set of situations R in k or less steps
        R_0=R
        R_(i+1)(x_0,..x_(n-1)) = 
          R_i(x_0,..x_(n-1))\/exists xn,..,x_(2n-1), T(x_0,..,x_(2n-1))/\R_i(x_n,..x_(2n-1))
    We will reach a fixpoint after a finite number of steps that is a solution of our problems.

    Of course the efficiency of this method depends on a more efficient representation of bdt 
    using reduced bdd,  sharing mechanisms and computation with memoization.
*) 


